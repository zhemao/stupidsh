1.
 a) Possible security problems in multi-user operating systems:
    - A program running under one user accessing sections of memory
      belonging to another user's process
    - A user gaining unauthorized access to another user's files
 b) We cannot ensure the same amount of security in a time-shared
    machine as in a dedicated machine. In a time-shared machine, it
    is always possible for an attacker to steal a user's access 
    credentials. For dedicated machines, this is not so much of a 
    problem, since access to the machine generally requires physical
    access to the computer hardware. 

2.
    On one hand, you could say it is impossible to construct a 
    secure operating system without hardware support for privileged
    and unprivileged modes because without hardware support, 
    a user program could bypass the security mechanisms the operating
    system has in place by running the illegal instructions directly.
    However, I think that it is possible, though somewhat complicated.
    You could have user programs run in a sort of hypervisor and only 
    allow the program to run the "user-mode" instructions. 
    Privileged instructions could only be run by requesting the kernel 
    to run it. You could distinguish between the two modes by setting a
    mode bit in some protected portion of memory. 
    Of course, the kernel would also have to check that user 
    programs do not contain illegal instructions before running them.

3. 
 a) A batch operating system takes in user programs and input in batch jobs
    and prints the results after all the computation is done.
 b) An interactive operating system allows direct communication between
    the user and running programs, generally through the form of input
    devices such as mice and keyboards.
 c) A time sharing operating system allows multiple users to run programs
    at the same time. The operating system must schedule the jobs in such
    a way that it appears to each user that he/she is the only one using
    the system.
 d) A real time operating system operates within strict time constraints.
    Real time operating systems guarantee that they will respond to events
    within a given amount of time. They are generally used in embedded systems.
 e) A network operating system is focused on running services shared
    over a computer network. The exposed services are generally file
    or printer sharing. Users of the network operating system connect
    to the server from client computers.
 f) Parallel operating systems are designed to run on hardware with
    multiple CPUs or multi-core CPUs. 
 g) Distributed operating systems are designed to coordinate the actions
    of multiple computers cooperating with each other over a network. 
    The different computers may have different roles or capabilities.
 h) A cluster operating system is designed to run on a computer cluster. 
    The operating system pools the resources of the many computers in the
    cluster together so that they appear to the user to be a single computer.
 i) A handheld operating system is designed to run on handheld devices, such
    as smart phones, tablets, or PDAs. They generally have a different user
    interface (touchscreen instead of mouse/keyboard) and must deal with
    limited hardware capabilities.

4. 
    One trade-off in handheld computers is between memory/storage size and 
    physical size. Putting more memory or disk storage into a handheld 
    computer requires more space, making the device bulkier and less portable.

    Another trade-off is between CPU performance and power comsumption. 
    A faster CPU requires more power and cooling, which means that either the
    battery must be made larger or battery life will suffer. 

5. 
    If you have limited disk space and the operating system does not need to 
    be extended or modified, it makes sense to store it in a ROM. Cell phones,
    game consoles, routers, and other "consumer devices" do this. 

    If you do want to extend or modify the operating system (for instance, 
    by installing new programs) and disk space is not an issue, it makes
    more sense to store the operating system on the disk. Most PCs do this.

6.
    The advantage of using the same system call interface for files and 
    devices is API simplicity and convenience. The programmer does not have
    to learn two different sets of system calls. The programmer is also able
    to write general code that works for both files and devices without 
    requiring special logic for each.

    The disadvantage is in security, since writing to a device can be more
    destructive than writing to a file. Also, some devices might have a 
    specific communication protocol. Thus, it becomes the operating system's
    (or the application programmer's) responsibility to ensure that programs
    don't send invalid data to the device.

7. 
    The advantage for OS designers is ease of debugging, since changes to
    the operating system can be tested without restarting the physical
    computer. The OS designer also doesn't have to worry about bugs in 
    their development code damaging the underlying system and putting 
    the computer into an unusable state.

    The advantage for the user is that they can be insulated from other users
    of the computer without sacrificing the convenience of a single-user 
    system. The user can install programs and perform other administrative
    tasks without having to ask an administrator. The user also doesn't have
    to worry about accidentally breaking the system, because they can always
    restore from a backup image.

8. 
    A just-in-time compiler is useful for executing java programs because 
    java programs are distributed as platform-independent bytecode. A JVM
    implementation that interprets the the bytecode directly would be slow,
    while a JVM implementation that uses ahead-of-time compilation would
    require users to wait through a potentially lengthy compilation. A JIT
    implementation balances the two, allowing speeds close to that of 
    native code without a long initial compilation.
