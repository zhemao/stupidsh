#ifndef __SH_STRUTILS_H__
#define __SH_STRUTILS_H__

#include <stdio.h>

/* a string array type, useful for splitting command lines */
struct str_array {
	char **strings;
	int cap;
	int length;
};

#define BUFFER_INIT_CAP 1024

struct str_buffer {
	char *str;
	int capacity;
	int length;
};

void destroy_array(struct str_array *array);
int init_array(struct str_array *array);
int add_string(struct str_array *array, char *str);
struct str_array *split_args(char *command);

/* remove trailing newlines and spaces from a string */
int chomp(char *str, int len);
/* make a newly malloced copy of a string */
char *copy(char *str);

int buffer_init(struct str_buffer *buf);
int buffer_getline(struct str_buffer *buf, FILE *f);
void buffer_destroy(struct str_buffer *buf);

#endif
