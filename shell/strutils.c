#include "strutils.h"

#include <stdlib.h>
#include <string.h>

int add_string(struct str_array *array, char *str)
{
	if (array->length >= (array->cap - 1)) {
		char **newstrings;

		array->cap *= 1.5;
		newstrings = realloc(array->strings,
					sizeof(char *) * array->cap);

		if (newstrings == NULL)
			return -1;

		array->strings = newstrings;
	}

	array->strings[array->length] = copy(str);

	if (array->strings[array->length] == NULL)
		return -1;

	array->length++;

	array->strings[array->length] = NULL;

	return 0;
}

void destroy_array(struct str_array *array)
{
	int i;

	for (i = 0; i < array->length; i++)
		free(array->strings[i]);

	free(array->strings);

	array->strings = NULL;
	array->cap = 0;
	array->length = 0;
}

int init_array(struct str_array *array)
{
	array->cap = 10;
	array->length = 0;

	array->strings = malloc(sizeof(char *) * array->cap);

	if (array->strings == NULL)
		return -1;

	array->strings[0] = NULL;

	return 0;
}

struct str_array *split_args(char *command)
{
	struct str_array *array;
	char *tok;
	int res;

	array = (struct str_array *) malloc(sizeof(struct str_array));

	if (array == NULL)
		return NULL;

	res = init_array(array);

	if (res != 0)
		return NULL;

	tok = strtok(command, " ");

	while (tok != NULL) {
		res = add_string(array, tok);

		if (res != 0) {
			destroy_array(array);
			free(array);
			return NULL;
		}

		tok = strtok(NULL, " ");
	}

	return array;
}

int chomp(char *str, int len)
{
	int i;

	for (i = len-1; i >= 0; i--) {
		if (str[i] != '\n' && str[i] != ' ') {
			str[i+1] = '\0';
			return i;
		}
	}

	str[0] = '\0';

	return 0;
}

char *copy(char *str)
{
	int len;
	char *newstr;

	len = strlen(str);

	newstr = (char *) malloc(len + 1);

	if (newstr == NULL)
		return NULL;

	strncpy(newstr, str, len);
	newstr[len] = '\0';

	return newstr;
}

int buffer_init(struct str_buffer *buf)
{
	buf->length = 0;
	buf->capacity = BUFFER_INIT_CAP;
	buf->str = (char *) malloc(buf->capacity);

	if (buf->str == NULL)
		return -1;
	return 0;
}

static int buffer_addchar(struct str_buffer *buf, char c)
{
	if (buf->length + 1 >= buf->capacity) {
		char *newstr;

		buf->capacity *= 1.5;
		newstr = realloc(buf->str, buf->capacity);

		if (newstr == NULL)
			return -1;

		buf->str = newstr;
	}

	buf->str[buf->length] = c;
	buf->length++;
	buf->str[buf->length] = '\0';

	return 0;
}

int buffer_getline(struct str_buffer *buf, FILE *f)
{
	int c, ret;

	buf->str[0] = '\0';
	buf->length = 0;

	while ((c = fgetc(f)) != '\n' && c != EOF) {
		ret = buffer_addchar(buf, c);
		if (ret != 0)
			return -1;
	}

	return buf->length;
}

void buffer_destroy(struct str_buffer *buf)
{
	free(buf->str);
	buf->length = buf->capacity = 0;
}
