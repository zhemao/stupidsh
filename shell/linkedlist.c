#include "linkedlist.h"
#include "strutils.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void llist_init(struct llist *llist)
{
	llist->first = NULL;
	llist->last = NULL;
	llist->length = 0;
}

void llist_destroy(struct llist *llist)
{
	struct llist_node *node = llist->first;
	struct llist_node *next;

	while (node != NULL) {
		next = node->next;

		node->next = NULL;

		free(node->str);
		free(node);

		node = next;
	}
}

int llist_add(struct llist *llist, char *cmd)
{
	struct llist_node *node = (struct llist_node *) malloc(
					sizeof(struct llist_node));

	if (node == NULL)
		return -1;

	node->str = copy(cmd);
	node->next = NULL;

	if (llist->length == 0) {
		llist->first = node;
		llist->last = node;
		llist->length = 1;
	} else {
		if (llist->length >= 100) {
			struct llist_node *first = llist->first;
			llist->first = first->next;
			free(first->str);
			free(first);
			llist->length--;
		}

		llist->last->next = node;
		llist->last = node;
		llist->length++;
	}

	return 0;
}

char *llist_nth(struct llist *llist, int n)
{
	int i;
	struct llist_node *node;

	node = llist->first;

	for (i = 0; i < n; i++) {
		if (node == NULL)
			return NULL;
		node = node->next;
	}

	return node->str;
}

int llist_remove(struct llist *llist, char *str)
{
	struct llist_node *node = llist->first;
	struct llist_node *prev = NULL;

	while (node != NULL) {
		if (strcmp(node->str, str) == 0)
			break;
		prev = node;
		node = node->next;
	}

	if (node == NULL)
		return -1;

	if (prev != NULL)
		prev->next = node->next;

	if (node == llist->first)
		llist->first = prev;

	if (node == llist->last)
		llist->last = prev;

	free(node->str);
	free(node);

	return 0;
}
