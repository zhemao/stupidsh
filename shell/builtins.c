#include "builtins.h"
#include "linkedlist.h"
#include "strutils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

struct builtin_entry builtins[NUM_BUILTINS] = {
	{ "cd", sh_chdir },
	{ "exit", sh_exit },
	{ "path", sh_path },
	{ "history", sh_history }
};

int sh_chdir(int argc, char **argv)
{
	char *dir;
	int res;

	if (argc == 2) {
		dir = argv[1];
	} else {
		fprintf(stderr, "error: wrong number of arguments\n");
		return -1;
	}

	res = chdir(dir);

	if (res < 0)
		perror("error");

	return res;
}

int sh_exit(int argc, char **argv)
{
	int retcode = 0;

	if (argc > 1)
		retcode = atoi(argv[1]);

	llist_destroy(&global_history);
	llist_destroy(&global_path);

	printf("exit\n");

	exit(retcode);
}

static void print_path(void)
{
	struct llist_node *node = global_path.first;

	while (node != global_path.last) {
		printf("%s:", node->str);
		node = node->next;
	}

	printf("%s\n", global_path.last->str);
}

int sh_path(int argc, char **argv)
{
	if (argc == 1) {
		print_path();
		return 0;
	}

	if (argc != 3) {
		fprintf(stderr, "Usage: path [+|- /some/dir]\n");
		return -1;
	}

	if (strcmp(argv[1], "+") == 0)
		return llist_add(&global_path, argv[2]);

	if (strcmp(argv[1], "-") == 0)
		return llist_remove(&global_path, argv[2]);

	fprintf(stderr, "Usage: path [+|- /some/dir]\n");
	return -1;
}

int sh_history(int argc, char **argv)
{
	struct llist_node *node = global_history.first;
	int n = 1;

	while (node != NULL) {
		printf("[%d] %s\n", n, node->str);
		node = node->next;
		n++;
	}

	return 0;
}

builtin_func find_builtin(const char *name)
{
	int i;

	for (i = 0; i < NUM_BUILTINS; i++) {
		if (strcmp(builtins[i].name, name) == 0)
			return builtins[i].builtin;
	}

	return NULL;
}
