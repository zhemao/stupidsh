#ifndef __SH_HISTORY_H__
#define __SH_HISTORY_H__

/* a singly-linked list type to store the command history and path */
struct llist_node {
	struct llist_node *next;
	char *str;
};

struct llist {
	struct llist_node *first;
	struct llist_node *last;
	int length;
};

void llist_init(struct llist *llist);
void llist_destroy(struct llist *llist);
int llist_add(struct llist *llist, char *command);
int llist_remove(struct llist *llist, char *command);
char *llist_nth(struct llist *llist, int n);

struct llist global_history;
struct llist global_path;

#endif
