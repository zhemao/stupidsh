#ifndef __SH_BUILTINS_H__
#define __SH_BUILTINS_H__

#define NUM_BUILTINS 4

typedef int (*builtin_func)(int, char **);

struct builtin_entry {
	const char *name;
	builtin_func builtin;
};

/* the builtin functions */
int sh_chdir(int argc, char **argv);
int sh_exit(int argc, char **argv);
int sh_path(int argc, char **argv);
int sh_history(int argc, char **argv);

/* find the builtin function associated with a given string */
builtin_func find_builtin(const char *name);

#endif
