#include "linkedlist.h"
#include "strutils.h"
#include "builtins.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>

#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>

char *search_path(char *cmd)
{
	struct llist_node *node;
	char *filename;
	int len;

	if (strchr(cmd, '/') != NULL) {
		if (access(cmd, F_OK) != 0)
			return NULL;

		filename = copy(cmd);

		if (filename == NULL)
			return NULL;

		return filename;
	}

	node = global_path.first;

	while (node != NULL) {
		/* dir + "/" + cmd */
		len = strlen(node->str) + strlen(cmd) + 1;
		filename = (char *) malloc(len + 1);

		if (filename == NULL)
			return NULL;

		snprintf(filename, len + 1, "%s/%s", node->str, cmd);

		if (access(filename, F_OK) == 0)
			return filename;

		free(filename);

		node = node->next;
	}

	return NULL;
}

pid_t spawn(char *program, char **args)
{
	pid_t pid = fork();

	if (pid < 0 || pid > 0)
		return pid;

	/* make sure forked process uses default signal handlers */
	signal(SIGINT, SIG_DFL);
	signal(SIGTSTP, SIG_DFL);

	execv(program, args);

	/* if we've reached this point, exec must have failed */
	perror("error");

	return 0;
}

char *readcmd(struct str_buffer *buf)
{
	ssize_t len;

	printf("$ ");

	len = buffer_getline(buf, stdin);

	if (len <= 0)
		return NULL;

	return buf->str;
}

int main(int argc, char **argv)
{
	char *line;
	struct str_array *args;
	struct str_buffer buf;
	int status, n;
	pid_t pid;
	char *cmd;
	builtin_func builtin;

	llist_init(&global_history);
	llist_init(&global_path);
	llist_add(&global_path, "/bin");
	llist_add(&global_path, "/usr/bin");

	printf("COMS W4118 Shell: Zhehao Mao (zm2169)\n");

	/* ignore interrupts and suspends inside shell */
	signal(SIGINT, SIG_IGN);
	signal(SIGTSTP, SIG_IGN);

	buffer_init(&buf);

	while ((line = readcmd(&buf)) != NULL) {

		if (strlen(line) == 0)
			continue;

		if (line[0] == '!') {
			n = atoi(line+1) - 1;
			line = llist_nth(&global_history, n);
		}

		llist_add(&global_history, line);

		args = split_args(line);

		/* first check if command is a builtin */
		builtin = find_builtin(args->strings[0]);

		if (builtin != NULL) {
			signal(SIGINT, SIG_DFL);
			signal(SIGTSTP, SIG_DFL);
			builtin(args->length, args->strings);
			signal(SIGINT, SIG_IGN);
			signal(SIGTSTP, SIG_IGN);
			destroy_array(args);
			free(args);
			continue;
		}

		/* find the command on the path */
		cmd = search_path(args->strings[0]);

		if (cmd == NULL) {
			fprintf(stderr, "error: Command not found: %s\n",
					args->strings[0]);

			destroy_array(args);
			free(args);
			continue;
		} else if (access(cmd, R_OK|X_OK) != 0) {
			perror("error");
			free(cmd);
			destroy_array(args);
			free(args);
			continue;
		}

		/* fork/exec the command and wait for forked process to exit */

		pid = spawn(cmd, args->strings);

		if (pid < 0)
			perror("error");
		else
			waitpid(pid, &status, 0);

		destroy_array(args);
		free(args);
		free(cmd);
	}

	buffer_destroy(&buf);
	llist_destroy(&global_history);
	llist_destroy(&global_path);
	return 0;
}
